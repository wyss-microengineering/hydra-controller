# Hydra Controller

This software package provides a versatile framework for networked control of the Interrogator fluid handler for Organ Chip experimentation. The user interface enables graphical programming of complex operations.